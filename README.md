# Databases for full-text searching

Full-text search databases are very good at quickly searching high volumes of unstructured, semi-structured, or structured text for a specific word or words. They have ranking capabilities to determine the best match for a search.

## Benefits of using a full-text search database
* These databases also provide natural language processing.
* Strong recommendation algorithms.
* Improved querying tools for text searching.
* Fuzzy search.

## When to Use a full-text search database
* The application will be indexing a high volume of primarily textual information.
* A high volume of text search queries submitted to the application.


## Elasticsearch vs Solr vs Lucene 
 |                  | Elasticsearch                                                              | Solr                       | Lucene                     |
 | ---------------- | -------------------------------------------------------------------------- | -------------------------- | -------------------------- |
 | Developer        | Elastic NV                                                                 | Apache Software Foundation | Apache Software Foundation |
 | Written In       | Java                                                                       | Java                       | Java                       |
 | Operating system | Cross-platform                                                             | Cross-platform             | Cross-platform             |
 | License          | Apache License 2.0 (partially; open source), Elastic License (proprietary) | Apache License 2.0         | Apache License 2.0         |



 ### Elasticsearch Features
 *  Elasticsearch can be used to search all kinds of documents.
 *  It provides scalable search, has near real-time search. 
 *  Elasticsearch is distributed, which means that indices can have zero or more replicas.


 ### Solr Features
 * Solr supports distributed group by (including grouped sorting, filtering, faceting). 
 * Solr exposes industry standard HTTP REST-like APIs with both XML and JSON support.


 ### Lucene Features
 * Lucene is recognized for its utility in the implementation of Internet search engines and local, single-site searching.
 * Lucene includes a feature to perform a fuzzy search.
 * Lucene has also been used to implement recommendation systems.


***

#### References
* [Lucid Works](https://lucidworks.com/post/full-text-search-engines-vs-dbms/)
* [Full Text Search Wiki](https://en.wikipedia.org/wiki/Full-text_search)
* [Elasticsearch Wiki](https://en.wikipedia.org/wiki/Elasticsearch)
* [Solr Wiki](https://en.wikipedia.org/wiki/Apache_Solr)
* [Lucene Wiki](https://en.wikipedia.org/wiki/Apache_Lucene)